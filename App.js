import React, { Component } from 'react';
import { View, Text,NativeModules, Platform } from 'react-native'; 
import Geo from './src/utils/Geo';
import Nav from './src/nav';

import RootStore from './src/mobx';
import UserStore from './src/mobx/userStore';

import { Provider } from 'mobx-react';
import JMessage from './src/utils/JMessage';
import AsyncStorage from '@react-native-async-storage/async-storage'; 
class App extends Component {
  state = {
    isInitGeo: false,
  }
  async componentDidMount() {
    if (Platform.OS == 'ios') {
      const {StatusBarManager} = NativeModules;
      StatusBarManager.getHeight(statusBar =>
        RootStore.setStatusBarHeight(statusBar.height) 
      );
    }
    const strUserInfo = await AsyncStorage.getItem("UserInfo");
    const userInfo = strUserInfo ? JSON.parse(strUserInfo): {};
    if(userInfo.token){
      RootStore.setUserInfo(userInfo.mobile, userInfo.token, userInfo.userId);
      JMessage.init();
    }
    const res0 = await Geo.initGeo(); 
    this.setState({isInitGeo: true})
    
  }
 
  render() {
    return (
        <View style={{flex:1}}>
          <Provider RootStore = { RootStore } UserStore = {UserStore}>
          {this.state.isInitGeo ? <Nav></Nav> : <></>}
          </Provider>
        </View>
    )
  }
}
export default App;