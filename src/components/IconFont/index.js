import React, { Component } from "react";
import {View, Text} from 'react-native';
import IconMaps from '../../res/fonts/icon'
const Index = (props)=> <Text onPress={props.onPress} style={{fontFamily: "iconfont", ...props.style}}>{IconMaps[props.name]}</Text>
export default Index;