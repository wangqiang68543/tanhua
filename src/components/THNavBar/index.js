import React, {Component} from 'react';
import {
  View,
  ImageBackground,
  Text,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import {pxToDp} from '../../utils/stylesKits';
import IconFont from '../IconFont';
import {NavigationContext} from '@react-navigation/native';
import {inject, observer} from 'mobx-react';

@inject('RootStore')
@observer
class Index extends Component {
  static contextType = NavigationContext;
  render() {
    const barH = this.props.RootStore.statusBarHeight;
    return (
      <View>
        <StatusBar backgroundColor="transparent" translucent={true}></StatusBar>
        <ImageBackground
          source={require('../../res/images/headbg.png')}
          style={{
            width: '100%',
            height: 44 + barH,
            justifyContent:"flex-end"
          }}>
          <View style={{
            width: '100%',
            height: 44,
            // paddingTop: pxToDp(12),
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',}}>
            <TouchableOpacity
              style={{
                paddingLeft: 8,
                flexDirection: 'row',
                alignItems: 'center',
                width: pxToDp(80),
              }}
              onPress={() => this.context.goBack()}>
              <IconFont name="iconfanhui" style={{color: '#fff'}} />
              <Text style={{color: '#fff'}}>返回</Text>
            </TouchableOpacity>
            <Text
              style={{color: '#fff', fontSize: pxToDp(20), fontWeight: 'bold'}}>
              {this.props.title}
            </Text>
            <Text style={{width: pxToDp(80)}}></Text>
          </View>
        </ImageBackground>
      </View>
    );
  }
}
export default Index;
