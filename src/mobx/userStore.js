import { observable, action, computed, makeObservable} from "mobx";
class UserStore {
    user = {}
    constructor() {
        // mobx6 和以前版本这是最大的区别 
        makeObservable(this, {
            user: observable, 
            setUser: action, 
            // titleName: computed
        });  
    }  
    setUser(user) {
        this.user = user; 
    } 
}

export default new UserStore();