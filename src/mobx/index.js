import { observable, action, computed, makeObservable} from "mobx";
import {StatusBar, Platform} from'react-native';
class RootStore {
    mobile = "";
    token = "";
    userId = "";
    statusBarHeight = Platform.OS == 'android' ? StatusBar.currentHeight : 20
    constructor() {
        // mobx6 和以前版本这是最大的区别 
        makeObservable(this, {
            mobile: observable,
            token: observable,
            userId: observable,
            statusBarHeight: observable,
            setUserInfo: action,
            setStatusBarHeight: action,
            // titleName: computed
        });  
    } 
    setStatusBarHeight(height) {
        console.log(`设置了高度${height}`)
        this.statusBarHeight = height;
    }
    setUserInfo(mobile, token, userId) {
        this.mobile = mobile;
        this.token = token;
        this.userId = userId;
    } 
}

export default new RootStore();