import {Dimensions, Platform, NativeModules, StatusBar} from 'react-native';

import { useSafeAreaInsets } from 'react-native-safe-area-context';

// 设计稿的宽度 / 元素的宽度 = 手机屏幕 / 手机中元素的宽度
// 手机中元素的宽度 = 手机屏幕 * 元素的宽度 / 设计稿的宽度
/**
 * 屏幕的宽度
 */
export const screenWidth = Dimensions.get('window').width;
/**
 * 屏幕的高度
 */
export const screenHeight = Dimensions.get('window').height;

/**
 * 将px转为dp
 * @param {Number} elePx 元素的宽度或高度 单位 px
 */
export const pxToDp = elePx => (screenWidth * elePx) / 375;

export function isIphoneX() {
  const window = Dimensions.get('window');
  return (
    Platform.OS === 'ios' &&
    !Platform.isPad &&
    !Platform.isTVOS &&
    [812, 896].includes(window.height)
  );
}

export const iosTop = isIphoneX() ? 50 : 20;

export const iosBottom = isIphoneX() ? 34 : 0;

export const isLessKitKat = Platform.OS === 'android' && Platform.Version < 19;

export const androidTop = isLessKitKat ? 0 : 24;

const {StatusBarManager} = NativeModules;
 

//   let statusBarHeight;
// 	if (Platform.OS === "ios") {
// 	     StatusBarManager.getHeight(height => {
// 	         statusBarHeight = height;
// 	     });
// 	 } else {
// 	     statusBarHeight = StatusBar.currentHeight;
// }
