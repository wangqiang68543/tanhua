// 接口文档 http://157.122.54.189:9089/swagger.html

/**
 * 接口基地址
 */
export const BASE_URI = 'http://157.122.54.189:9089';

/**
 *  登录 获取验证码
 */
export const ACCOUNT_LOGIN = '/user/login'; // 登录

/**
 * 检查验证码
 */
export const ACCOUNT_VALIDATEVCODE = '/user/loginVerification'; // 检查验证码

/**
 * 新用户---填写资料
 */
 export const USER_REGINFO = '/user/loginReginfo'; // 新用户---填写资料

 /**
 * 审核头像
 */
 export const ACCOUNT_CHECKHEADIMAGE = '/user/loginReginfo/head'; //审核头像

/**
 * 推荐朋友
 */
export const FRIENDS_RECOMMENDATION = '/friends/recommendation'; // 推荐朋友

/**
 * 探花-左滑右滑
 */
export const FRIENDS_CARDS = '/friends/cards'; // 探花-左滑右滑

/**
 * 最近来访
 */
export const FRIENDS_VISITORS = '/friends/visitors'; // 最近来访

/**
 * 搜索附近
 */
export const FRIENDS_SEARCH = '/friends/search'; // 搜索附近

/**
 * 探花-喜欢和不喜欢
 */
 export const FRIENDS_LIKE = '/friends/like'; // 探花-喜欢和不喜欢

 /**
  * 朋友信息（点击朋友进入）
  */
 export const FRIENDS_PERSONALINFO = '/friends/personalInfo/:id'; // 朋友信息（点击朋友进入）
 
 /**
  * 推测灵魂-问卷列表
  */
 export const FRIENDS_QUESTIONS = '/friends/questions'; // 测灵魂-问卷列表

 /**
 * 测灵魂-提交问卷获得鉴定单信息
 */
export const FRIENDS_QUESIONSANS = '/friends/questionsAns'; // 测灵魂-提交问卷获得鉴定单信息

/**
 * 今日佳人
 */
export const FRIENDS_TODAYBEST = '/friends/todayBest'; // 今日佳人

/**
 * 测灵魂- 问卷试题
 */
export const FRIENDS_QUESTIONSECTION = '/friends/questionSection'; // 测灵魂- 问卷试题

/**
 *  推荐动态
 */
 export const QZ_RECOMMEND = '/qz/recommend'; //  推荐动态

/**
 *  最新动态
 */
 export const QZ_NEWTRENDS = '/qz/newtrends'; //  最新动态

/**
 *  单条动态-评论列表
 */
 export const QZ_COMMENT = '/qz/comment'; //  单条动态-评论列表

/**
 *  动态-点赞&取消点赞
 */
 export const QZ_STAR = '/qz/star'; //  动态-点赞&取消点赞                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   

/**
 *  动态-喜欢&取消喜欢
 */
 export const QZ_LIKE = '/qz/like'; //  动态-喜欢&取消喜欢

/**
 *  动态-不感兴趣
 */
 export const QZ_NOINTEREST = '/qz/noInterest'; //  动态-不感兴趣

/**
 *  评论-点赞
 */
 export const QZ_COMMENTS_STAR = '/qz/comments/star'; //  评论-点赞

/**
 *  评论-提交
 */
 export const QZ_COMMENTS_SUBMIT = '/qz/comments/submit'; //  评论-提交

/**
 *  动态-发布
 */
 export const QZ_TREND_SUBMIT = '/qz/trend/submit'; //  动态-发布

/**
 *  动态-图片-上传
 */
 export const QZ_TREND_IMAGE_UPLOAD = '/qz/trends/image/upload'; //  动态-图片-上传

/**
 *  我的个人信息
 */
 export const MY_USERINFO = '/my/userinfo'; // 我的个人信息

/**
 *  我的动态
 */
 export const MY_TRENDS = '/my/trends'; // 我的动态

/**
 *  互相喜欢，喜欢，粉丝 - 统计
 */
 export const MY_COUNTS = '/my/counts'; // 互相喜欢，喜欢，粉丝 - 统计

/**
 *  互相喜欢，喜欢，粉丝 - 列表数据
 */
 export const MY_LIKELIST = '/my/likelist'; // 互相喜欢，喜欢，粉丝 - 列表数据

/**
 *  保存个人信息
 */
 export const MY_SUBMITUSERINFO = '/my/submitUserInfo'; // 保存个人信息

/**
 *  点赞列表
 */
 export const MESSAGE_STARLIST = '/message/starlist'; // 点赞列表

