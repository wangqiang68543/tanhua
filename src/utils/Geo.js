import {PermissionsAndroid, PermissionsiOS, Platform} from 'react-native';
import {init, Geolocation} from 'react-native-amap-geolocation';
import axios from 'axios';
import Toast from './Toast';
class Geo {
  async initGeo() {
    if (Platform.OS === 'android') {
      await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
      );
    } 
    await init({
      // 来自于 高德地图中的第二个应用 android 应用key
      ios: '6235b6897048e69eb6c0def47f0282cc',
      android: '6235b6897048e69eb6c0def47f0282cc',
    });
    return Promise.resolve();
  }
  async getCurrentPosition() {
    return new Promise((resolve, reject) => {
      console.log('开始定位');
      Geolocation.getCurrentPosition(({coords}) => {
        resolve(coords);
      }, reject);
    });
  }
  async getCityByLocation() {
    Toast.showLoading('努力获取中');
    const {longitude, latitude} = await this.getCurrentPosition();
    const res = await axios.get('https://restapi.amap.com/v3/geocode/regeo', {
      // key  高德地图中第一个应用的key
      params: {
        location: `${longitude},${latitude}`,
        key: 'fff5cbff36d612bb716d1907f5b79b13',
      },
    });
    Toast.hideLoading();
    return Promise.resolve(res.data);
  }
}

export default new Geo();
