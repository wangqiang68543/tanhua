import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity, Modal} from 'react-native';
import request from '../../../utils/request';
import {FRIENDS_PERSONALINFO, BASE_URI} from '../../../utils/pathMap';
import {ImageHeaderScrollView} from 'react-native-image-header-scroll-view';
import {inject, observer} from 'mobx-react';
import {pxToDp} from '../../../utils/stylesKits';
import {Carousel} from 'teaset';
import IconFont from '../../../components/IconFont';
import LinearGradient from 'react-native-linear-gradient';
import ImageViewer from 'react-native-image-zoom-viewer';
@inject('RootStore')
@observer
class Index extends Component {
  state = {
    userDetail: {},
    trends:[],
    showAlbum: false,
    currentIndex: 0,
    imgUrls:[],
  };
  params = {
    page: 1,
    pagesize: 10,
  };
  totalPages=1;
  isLoading=false;
  async componentDidMount() {
      console.log("----------------")
    this.getDetail();
  }
  getDetail = async () => {
    const url = FRIENDS_PERSONALINFO.replace(':id', this.props.route.params.id);
    const res = await request.privateGet(url, this.params);
    this.totalPages = res.pages;
    this.setState({userDetail: res.data, trends: [...this.state.trends, ...res.data.trends]});
    this.isLoading = false;
  };
  //点击显示相册大图
  handleShowAlbum = (i, ii) => {
    const imgUrls = this.state.trends[i].album.map(v=>({url: BASE_URI+v.thum_img_path}));
    const currentIndex = ii;
    const showAlbum = true;
    this.setState({showAlbum, imgUrls, currentIndex});
  }
  onScroll = ({nativeEvent}) => { 
        const isReachBottom = nativeEvent.contentOffset.y + nativeEvent.layoutMeasurement.height >= nativeEvent.contentSize.height;
        const hasMore = this.totalPages> this.params.page;
        if (isReachBottom&&hasMore&&!this.isLoading) {
            this.params.page++;
            this.isLoading = true;
            console.log("触底")
            this.getDetail();
        }
  }
  render() {
    const statusBarHeight = this.props.RootStore.statusBarHeight;
    const {userDetail} = this.state;
    const {showAlbum, imgUrls, currentIndex, trends} = this.state;
    console.log(imgUrls);
    if (!userDetail.silder) return <></>;
    return (
      <ImageHeaderScrollView
        maxHeight={pxToDp(200) + statusBarHeight}
        minHeight={pxToDp(44) + statusBarHeight}
        onScroll={this.onScroll}
        headerImage={require('../../../res/images/headfriend.png')}
        renderForeground={() => (
          <Carousel control style={{height: '100%', width: '100%'}}>
            {userDetail.silder.map((v, i) => (
              <Image
                key={i}
                source={{uri: BASE_URI + v.thum_img_path}}
                style={{height: '100%', width: '100%'}}
              />
            ))}
          </Carousel>
        )}>
        {/* 个人信息 */}
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            borderBottomColor: '#ccc',
            borderBottomWidth: 1,
            paddingBottom: pxToDp(5),
            paddingTop: pxToDp(5),
          }}>
          <View
            style={{
              flex: 3,
              flexDirection: 'column',
              justifyContent: 'space-around',
              paddingLeft: pxToDp(10),
            }}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={{color: '#555'}}>{userDetail.nick_name}</Text>
              <IconFont
                name={
                  userDetail.gender === '女' ? 'icontanhuanv' : 'icontanhuanan'
                }
                style={{
                  fontSize: pxToDp(18),
                  color: userDetail.gender === '女' ? '#b564bf' : 'red',
                  marginLeft: pxToDp(5),
                  marginRight: pxToDp(5),
                }}
              />
              <Text style={{color: '#555', marginRight: pxToDp(5)}}>
                {userDetail.age}岁
              </Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={{color: '#555', marginRight: pxToDp(5)}}>
                {userDetail.marry}|{userDetail.xueli}|
                {userDetail.agediff < 10 ? '年龄相仿' : '有点代沟'}
              </Text>
            </View>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'column',
            }}>
            <View
              style={{
                position: 'relative',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <IconFont
                name="iconxihuan"
                style={{fontSize: pxToDp(50), color: 'red'}}
              />
              <Text
                style={{
                  position: 'absolute',
                  fontSize: pxToDp(13),
                  fontWeight: 'bold',
                  color: '#fff',
                }}>
                {userDetail.fateValue}
              </Text>
            </View>
            <Text style={{fontSize: pxToDp(16), color: 'red'}}>缘分值</Text>
          </View>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            paddingLeft: pxToDp(10),
            paddingRight: pxToDp(10),
            paddingTop: pxToDp(5),
            paddingBottom: pxToDp(5),
            alignItems: 'center',
            borderBottomColor: '#ccc',
            borderBottomWidth: 1,
          }}>
          {/* 标题 */}
          <View
            style={{
              flexDirection: 'row',
              padding: pxToDp(10),
              justifyContent: 'space-between',
            }}>
            <Text style={{color: '#666'}}>动态</Text>
            <View
              style={{
                backgroundColor: 'red',
                width: pxToDp(22),
                height: pxToDp(22),
                borderRadius: pxToDp(11),
                alignItems: 'center',
                justifyContent: 'center',
                marginLeft: pxToDp(5),
              }}>
              <Text style={{color: '#fff'}}>{trends.length}</Text>
            </View>
          </View>
          {/* 按钮 */}
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity style={{marginRight: pxToDp(8)}}>
              <LinearGradient
                colors={['#f2ab5a', '#ec7c50']}
                start={{x: 0, y: 0}}
                end={{x: 1, y: 0}}
                style={{
                  flexDirection: 'row',
                  width: pxToDp(100),
                  height: pxToDp(30),
                  borderRadius: pxToDp(15),
                  alignItems: 'center',
                  justifyContent: 'space-evenly',
                }}>
                <IconFont
                  name="iconliaotian"
                  style={{color: '#fff'}}></IconFont>
                <Text style={{color: '#fff'}}>聊一下</Text>
              </LinearGradient>
            </TouchableOpacity>

            <TouchableOpacity style={{marginRight: pxToDp(8)}}>
              <LinearGradient
                colors={['#6d47fb', '#e56b7f']}
                start={{x: 0, y: 0}}
                end={{x: 1, y: 0}}
                style={{
                  flexDirection: 'row',
                  width: pxToDp(100),
                  height: pxToDp(30),
                  borderRadius: pxToDp(15),
                  alignItems: 'center',
                  justifyContent: 'space-evenly',
                }}>
                <IconFont
                  name="iconxihuan-o"
                  style={{color: '#fff'}}></IconFont>
                <Text style={{color: '#fff'}}>喜欢</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>

        {/* 列表 */}
        <View>
          {trends.map((v, i) => (
            <View key={i} style={{padding: pxToDp(10)}}>
              <View
                style={{
                  flexDirection: 'row',
                }}>
                <Image
                  style={{
                    width: pxToDp(40),
                    height: pxToDp(40),
                    borderRadius: pxToDp(20),
                  }}
                  source={{uri: BASE_URI + userDetail.header}}
                />
                <View
                  style={{
                    flexDirection: 'column',
                    justifyContent: 'space-between',
                    paddingLeft: pxToDp(5),
                  }}>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Text style={{color: '#555'}}>{userDetail.nick_name}</Text>
                    <IconFont
                      name={
                        userDetail.gender === '女'
                          ? 'icontanhuanv'
                          : 'icontanhuanan'
                      }
                      style={{
                        fontSize: pxToDp(18),
                        color: userDetail.gender === '女' ? '#b564bf' : 'red',
                      }}
                    />
                    <Text style={{color: '#555'}}>{userDetail.age}岁</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text style={{color: '#555'}}>
                      {userDetail.marry}|{userDetail.xueli}|
                      {userDetail.agediff < 10 ? '年龄相仿' : '有点代沟'}
                    </Text>
                  </View>
                </View>
              </View>

              <View style={{marginTop: pxToDp(8)}}>
                <Text style={{color: '#666'}}>{v.content}</Text>
              </View>
              <View style={{flexWrap:"wrap", flexDirection:"row", paddingBottom:pxToDp(5), paddingTop:pxToDp(5)}}>
                {v.album.map((vv, ii) => (
                    <TouchableOpacity key={{ii}} onPress={()=>this.handleShowAlbum(i,ii)}> 
                  <Image 
                    style={{width: pxToDp(70), height: pxToDp(70),marginRight:pxToDp(5)}}
                    source={{uri: BASE_URI + vv.thum_img_path}}
                  />
                  </TouchableOpacity>
                ))}
              </View>
            </View>
          ))}
          
        </View>
        {this.params.page >= this.totalPages ? <View style={{alignItems:"center"}}><Text>没有更多数据了</Text></View>: <></>}
        <Modal visible={showAlbum} transparent={true} >
                <ImageViewer imageUrls={imgUrls} index={currentIndex} onClick={() => this.setState({showAlbum:false})}/>
            </Modal>

      </ImageHeaderScrollView>
    );
  }
}
export default Index;
