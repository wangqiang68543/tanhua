import React, {Component} from 'react';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native';
import THNavBar from '../../../components/THNavBar';
import Swiper from 'react-native-swiper';
import request from '../../../utils/request';
import {FRIENDS_CARDS, BASE_URI} from '../../../utils/pathMap';
import {pxToDp} from '../../../utils/stylesKits';
import IconFont from '../../../components/IconFont';
class Index extends Component {
  state = {
    cards: [],
    params: {
      page: 1,
      pagesize: 10,
    },
  };
  getFriendCards = async () => {
    const res = await request.privateGet(FRIENDS_CARDS, this.state.params);
    this.setState({cards: res.data});
  };
  componentDidMount() {
    this.getFriendCards();
  }
  goChat = () => {
    const {userDetail} = this.state;
    this.props.navigation.navigate("Chat", userDetail);
  }
  render() {
    const {cards} = this.state;
    if (cards.length == 0) {
      return <></>;
    }
    return (
      <View style={{flex: 1, color: '#fff'}}>
        <THNavBar title="探花"></THNavBar>
        <View style={{flex: 1, position: 'relative', alignItems: 'center'}}>
          <ImageBackground
            style={{
              height: '60%',
              width: '100%',
              position: 'absolute',
              left: 0,
              top: 0,
            }}
            imageStyle={{height: '100%'}}
            source={require('../../../res/images/testsoul_bg.png')}></ImageBackground>
            <View style={{backgroundColor:'red', width:"100%", height: '85%',}}> 
          <Swiper
            style={{
              backgroundColor: 'transparent',
              backgroundColor:'green',
              height: '100%',
            }}
            showsButtons={false}
            showsPagination={false}>
            {cards.map((v, i) => (
              <View
                key={i}
                style={{
                  height: '100%',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    height: '85%',
                    backgroundColor: 'transparent',
                    width: '85%',
                    borderColor: '#999',
                    borderWidth: 1,
                  }}>
                  <Image
                    source={{uri: BASE_URI + v.header}}
                    style={{flex: 6, width: '100%'}}></Image>

                  <View
                    style={{
                      flexDirection: 'column',
                      justifyContent: 'space-around',
                      paddingLeft: pxToDp(5),
                      alignItems: 'center',
                      flex: 1,
                      width: '100%',
                    }}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Text style={{color: '#555'}}>{v.nick_name}</Text>
                      <IconFont
                        name={
                          v.gender === '女' ? 'icontanhuanv' : 'icontanhuanan'
                        }
                        style={{
                          fontSize: pxToDp(18),
                          color: v.gender === '女' ? '#b564bf' : 'red',
                        }}
                      />
                      <Text style={{color: '#555'}}>{v.age}岁</Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Text style={{color: '#555'}}>
                        {v.marry}|{v.xueli}|
                        {v.agediff < 10 ? '年龄相仿' : '有点代沟'}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            ))}
          </Swiper>
          </View>
          <View
            style={{
              width: '60%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              style={{
                backgroundColor: '#ebc869',
                width: pxToDp(60),
                height: pxToDp(60),
                borderRadius: pxToDp(30),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <IconFont
                name="iconbuxihuan"
                style={{fontSize: pxToDp(30), color: '#fff'}}></IconFont>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                backgroundColor: 'red',
                width: pxToDp(60),
                height: pxToDp(60),
                borderRadius: pxToDp(30),
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <IconFont
                name="iconxihuan"
                style={{fontSize: pxToDp(30), color: '#fff'}}></IconFont>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
export default Index;
