import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import {pxToDp} from '../../../../utils/stylesKits';
import request from '../../../../utils/request';
import {FRIENDS_TODAYBEST, BASE_URI} from '../../../../utils/pathMap';
import IconFont from '../../../../components/IconFont';
class Index extends Component {
  state = {
    perfectGirl: {
      //     "dis":246.2,
      //     "age":23,
      //     "agediff":0,
      //     "fateValue":78,
      //     "gender":"女",
      //     "header":"/upload/13828459788.jpg",
      //     "marry":"单身",
      //     "nick_name":"若只如初见",
      //     "id":16,
      //     "xueli":"大专"
    },
  };
  async componentDidMount() {
    const res = await request.privateGet(FRIENDS_TODAYBEST); 
    this.setState({
      perfectGirl: {
        dis: 246.2,
        age: 23,
        agediff: 0,
        fateValue: 78,
        gender: '女',
        header: '/upload/13828459788.jpg',
        marry: '单身',
        nick_name: '若只如初见',
        id: 16,
        xueli: '大专',
      },
    });
    // this.setState({visitors: res.data});
  }
  render() {
    const {perfectGirl} = this.state;
    return (
      <View
        style={{
          flexDirection: 'row',
          paddingLeft: pxToDp(5),
          paddingRight: pxToDp(5),
        }}>
        <View style={{position: 'relative'}}>
          <Image
            style={{height: pxToDp(120), width: pxToDp(120)}}
            source={{uri: BASE_URI + perfectGirl.header}}
          />
          <View
            style={{
              width: pxToDp(80),
              height: pxToDp(30),
              backgroundColor: '#b564bf',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: pxToDp(10),
              position: 'absolute',
              left: 0,
              bottom: pxToDp(10),
            }}>
            <Text style={{color: '#fff', fontSize: pxToDp(16)}}>今日佳人</Text>
          </View>
        </View>

        <View
          style={{
            flex: 1,
            flexDirection: 'row',
          }}>
          <View
            style={{
              flex: 3,
              flexDirection: 'column',
              justifyContent: 'space-around',
              paddingLeft: pxToDp(5),
            }}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={{color: '#555'}}>{perfectGirl.nick_name}</Text>
              <IconFont
                name={
                  perfectGirl.gender === '女' ? 'icontanhuanv' : 'icontanhuanan'
                }
                style={{
                  fontSize: pxToDp(18),
                  color: perfectGirl.gender === '女' ? '#b564bf' : 'red',
                }}
              />
              <Text style={{color: '#555'}}>{perfectGirl.age}岁</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={{color: '#555'}}>
                {perfectGirl.marry}|{perfectGirl.xueli}|{perfectGirl.agediff < 10 ? "年龄相仿":"有点代沟"}
              </Text>
            </View>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'column',
            }}>
            <View style={{position: 'relative', alignItems:'center', justifyContent:'center'}}>
              <IconFont
                name="iconxihuan"
                style={{fontSize: pxToDp(50), color: 'red'}}
              />
              <Text
                style={{
                  position: 'absolute', 
                  fontSize: pxToDp(13),
                  fontWeight: 'bold',
                  color: '#fff',
                }}>
                {perfectGirl.fateValue}
              </Text>
            </View>
            <Text style={{fontSize: pxToDp(16), color: 'red'}}>缘分值</Text>
          </View>
        </View>
      </View>
    );
  }
}
export default Index;
