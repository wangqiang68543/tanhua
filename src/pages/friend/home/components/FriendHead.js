import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {tanhua, near, testSoul} from '../../../../res/fonts/iconSvg';
import {SvgXml} from 'react-native-svg';
import {pxToDp} from '../../../../utils/stylesKits';
import { NavigationContext } from '@react-navigation/native';
class Index extends Component { 
  static contextType = NavigationContext;
  goPage = (page) => { 
    // We can access navigation object via context  
    this.context.navigate(page);
  }
  render() {
    return (
      <View style={{flexDirection:'row', width:"80%", justifyContent:"space-around"}}>
        <TouchableOpacity style={{alignItems: 'center'}} onPress={()=>this.goPage("TanHua")}>
          <View
            style={{
              width: pxToDp(70),
              height: pxToDp(70),
              borderRadius: pxToDp(35),
              backgroundColor: 'red',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <SvgXml width="40" height="40" fill="#fff" xml={tanhua}></SvgXml>
          </View>
          <Text
              style={{
                fontSize: pxToDp(18),
                marginTop: pxToDp(4),
                color: '#ffffff9a',
              }}>
              探花
            </Text>
        </TouchableOpacity>

        <TouchableOpacity style={{alignItems: 'center'}}>
          <View
            style={{
              width: pxToDp(70),
              height: pxToDp(70),
              borderRadius: pxToDp(35),
              backgroundColor: '#2db3f8',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <SvgXml width="40" height="40" fill="#fff" xml={near}></SvgXml> 
          </View>
          <Text
              style={{
                fontSize: pxToDp(18),
                marginTop: pxToDp(4),
                color: '#ffffff9a',
              }}>
              搜附近
            </Text>
        </TouchableOpacity>

        <TouchableOpacity style={{alignItems: 'center'}}>
          <View
            style={{
              width: pxToDp(70),
              height: pxToDp(70),
              borderRadius: pxToDp(35),
              backgroundColor: '#ecc768',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <SvgXml width="40" height="40" fill="#fff" xml={testSoul}></SvgXml>
          </View>
          <Text
              style={{
                fontSize: pxToDp(18),
                marginTop: pxToDp(4),
                color: '#ffffff9a',
              }}>
              测灵魂
            </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
export default Index;
