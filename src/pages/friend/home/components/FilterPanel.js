import React, {Component} from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import IconFont from '../../../../components/IconFont';
import {pxToDp} from '../../../../utils/stylesKits';
import {SvgXml} from 'react-native-svg';
import {male, female} from '../../../../res/fonts/iconSvg';
import Picker from 'react-native-picker';
import {Slider} from 'react-native-elements';
import CityJson from '../../../../res/citys.json';
import THButton from '../../../../components/THButton';
class Index extends Component {
  constructor(props) {
    super(props);
    this.state = JSON.parse(JSON.stringify(this.props.params));
  }
  chooseGender = gender => {
    this.setState({gender});
  };
  chooseLastLogin = () => {
    Picker.init({
      pickerData: ['15分钟', '1天', '1小时', '不限制'],
      selectedValue: [this.state.lastLogin],
      wheelFlex: [1, 0, 0],
      pickerConfirmBtnText: '确定',
      pickerCancelBtnText: '取消',
      pickerTitleText: '近期登录时间',
      onPickerConfirm: data => {
        console.log(data);
        this.setState({lastLogin: data[0]});
      },
      onPickerCancel: data => {
        console.log(data);
      },
      onPickerSelect: data => {
        console.log(data);
      },
    });
    Picker.show();
  };
  chooseCity = () => {
    Picker.init({
      pickerData: CityJson,
      selectedValue: ['北京', '北京'],
      wheelFlex: [1, 1, 0],
      pickerConfirmBtnText: '确定',
      pickerCancelBtnText: '取消',
      pickerTitleText: '选择城市',
      onPickerConfirm: data => {
        console.log(data);
        this.setState({city: data[1]});
      },
      onPickerCancel: data => {
        console.log(data);
      },
      onPickerSelect: data => {
        console.log(data);
      },
    });
    Picker.show();
  };
  chooseEducation = () => {
    Picker.init({
      pickerData: [
        '博士后',
        '博士',
        '硕士',
        '本科',
        '大专',
        '高中',
        '留学',
        '其他',
      ],
      selectedValue: ['其他'],
      wheelFlex: [1, 0, 0],
      pickerConfirmBtnText: '确定',
      pickerCancelBtnText: '取消',
      pickerTitleText: '选择学历',
      onPickerConfirm: data => {
        console.log(data);
        this.setState({education: data[0]});
      },
    });
    Picker.show();
  };
  handleSubmit = () => { 
      this.props.onSubmitFilter(this.state);
      this.props.onClose();
  };
  render() {
    const {gender, lastLogin, distance, city, education} = this.state;
    return (
      <View
        style={{
          backgroundColor: '#fff',
          width: '100%',
          height: '70%',
          position: 'absolute',
          left: 0,
          bottom: 0,
          paddingLeft: pxToDp(10),
          paddingRight: pxToDp(10),
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            height: pxToDp(50),
          }}>
          <Text></Text>
          <Text
            style={{fontSize: pxToDp(28), fontWeight: 'bold', color: '#999'}}>
            筛选
          </Text>
          <IconFont
            onPress={this.props.onClose}
            style={{fontSize: pxToDp(30)}}
            name="iconguanbi"></IconFont>
        </View>

        {/* 性别 */}
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: pxToDp(10),
          }}>
          <Text
            style={{
              fontSize: pxToDp(18),
              fontWeight: 'bold',
              color: '#777',
              width: pxToDp(80),
            }}>
            性别:
          </Text>
          <View
            style={{
              width: '60%',
              flexDirection: 'row',
              alignSelf: 'center',
              justifyContent: 'space-around',
            }}>
            <TouchableOpacity
              style={{
                height: pxToDp(60),
                width: pxToDp(60),
                borderRadius: pxToDp(30),
                backgroundColor: gender === '男' ? 'red' : '#eee',
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={this.chooseGender.bind(this, '男')}>
              <SvgXml xml={male} width="34" height="34"></SvgXml>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                height: pxToDp(60),
                width: pxToDp(60),
                borderRadius: pxToDp(30),
                backgroundColor: gender === '女' ? 'red' : '#eee',
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={this.chooseGender.bind(this, '女')}>
              <SvgXml xml={female} width="34" height="34"></SvgXml>
            </TouchableOpacity>
          </View>
        </View>

        {/* 近期登录时间 */}
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: pxToDp(10),
          }}>
          <Text
            style={{fontSize: pxToDp(18), width: pxToDp(140), color: '#999'}}>
            近期登录时间:
          </Text>
          <Text
            onPress={this.chooseLastLogin}
            style={{fontSize: pxToDp(18), color: '#999'}}>
            {lastLogin || '请选择'}
          </Text>
        </View>

        {/* 选择距离 */}
        <View style={{marginTop: pxToDp(10)}}>
          <Text style={{fontSize: pxToDp(18), color: '#777'}}>
            距离:{distance || 0} KM
          </Text>
          <Slider
            value={distance}
            minimumValue={0}
            maximumValue={10}
            step={0.5}
            onValueChange={distance => this.setState({distance})}></Slider>
        </View>

        {/* 居住地 */}
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: pxToDp(10),
          }}>
          <Text
            style={{fontSize: pxToDp(18), width: pxToDp(100), color: '#999'}}>
            居住地:
          </Text>
          <Text
            onPress={this.chooseCity}
            style={{fontSize: pxToDp(18), color: '#999'}}>
            {city || '请选择'}
          </Text>
        </View>

        {/* 学历 */}
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: pxToDp(10),
          }}>
          <Text
            style={{fontSize: pxToDp(18), width: pxToDp(90), color: '#999'}}>
            学历:
          </Text>
          <Text
            onPress={this.chooseEducation}
            style={{fontSize: pxToDp(18), color: '#999'}}>
            {education || '请选择'}
          </Text>
        </View>
        {/* 确认按钮 */}
        <THButton
          style={{
            borderRadius: pxToDp(10),
            height: pxToDp(40),
            width: '50%',
            alignSelf: 'center',
            marginTop: pxToDp(10)
          }}
          onPress={this.handleSubmit}>
          确认
        </THButton>
      </View>
    );
  }
}
export default Index;
