import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import {pxToDp} from '../../../../utils/stylesKits';
import request from '../../../../utils/request';
import {FRIENDS_VISITORS, BASE_URI} from '../../../../utils/pathMap';

class Index extends Component {
  state = {
    visitors: [
      // {
      //     "Distance":9666804.2,
      //     "age":21,
      //     "agediff":-2,
      //     "fateValue":62,
      //     "gender":"女",
      //     "header":"/upload/13828459782.png",
      //     "marry":"未婚",
      //     "nick_name":"雾霭朦胧",
      //     "target_uid":7,
      //     "uid":8,
      //     "xueli":"大专"
      // }
    ],
  };
  async componentDidMount() {
    const res = await request.privateGet(FRIENDS_VISITORS); 
    this.setState({visitors: res.data});
  }
  render() {
    const {visitors} = this.state;
    let peoples = visitors.slice(0, 3);
    return (
      <View
        style={{
          flexDirection: 'row',
          paddingLeft: pxToDp(5),
          paddingRight: pxToDp(5),
          marginTop: pxToDp(20),
        //   justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <Text style={{flex: 1, color: '#777', fontSize: pxToDp(15)}}>
          最近有{visitors.length}人来访,快去看看
        </Text>
        <View
          style={{
            flexDirection: 'row',
            flex: 1,
            alignItems: 'center',
            justifyContent: 'space-around', 
          }}>
          {peoples.map((v, i) => {
            return (
              <Image
                key= {i}
                style={{
                  width: pxToDp(50),
                  height: pxToDp(50),
                  borderRadius: pxToDp(25),
                }}
                source={{uri: BASE_URI + v.header}}
              />
            );
          })}
          <Text style={{color: '#777', fontSize: pxToDp(20)}}>&gt;</Text>
        </View>
      </View>
    );
  }
}
export default Index;
