import React, {Component} from 'react';
import {Platform} from 'react-native';
import {
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  SafeAreaView,
  NativeModules,
  Image,
} from 'react-native';
import {ImageHeaderScrollView} from 'react-native-image-header-scroll-view';
import {pxToDp} from '../../../utils/stylesKits';
import FriendHead from './components/FriendHead';
import Vistiors from './components/Vistiors';
import PerfectGirl from './components/PerfectGirl';
// import { useSafeAreaInsets } from 'react-native-safe-area-context';
import request from '../../../utils/request';
import {FRIENDS_RECOMMENDATION, BASE_URI} from '../../../utils/pathMap';
import IconFont from '../../../components/IconFont';
import {Overlay} from 'teaset';
import FilterPanel from './components/FilterPanel';
import { NavigationContext } from '@react-navigation/native';
class Index extends Component {
  static contextType = NavigationContext;
  state = {
    statusBarHeight: Platform.OS == 'android' ? StatusBar.currentHeight : 20,
    params: {
      page: 1,
      pagesize: 10,
      gender: '男',
      distance: 2,
      lastLogin: '',
      city: '',
      education: '',
    },
    recommends: [
      //   {
      //     "age":23,
      //     "agediff":0,
      //     "dist":0,
      //     "fateValue":40,
      //     "gender":"男",
      //     "header":"/upload/162151615708018665711978.jpg",
      //     "id":7,
      //     "marry":"单身",
      //     "nick_name":"admin",
      //     "xueli":"本科"
      // }
    ],
  };
  getRecommends = async (filterParmas={}) => { 
    const res = await request.privateGet(
      FRIENDS_RECOMMENDATION,
      {...this.state.params, ...filterParmas},
    );
    this.setState({recommends: res.data});
  };
  recommendFilterShow=()=> {

    const {page, pagesize, ...others} = this.state.params;

    let overlayViewRef = null;
    let overlayView = (
      <Overlay.View 
        style={{flex: 1, flexDirection:'column', position:"relative"}}
        modal={true}
        overlayOpacity={0.5}
        ref={v => (overlayViewRef = v)}>
          <FilterPanel onSubmitFilter={this.handleSubmitFilter} params={others}  onClose={()=>overlayViewRef.close()}></FilterPanel>
      </Overlay.View>
    );
    Overlay.show(overlayView);
  }
  handleSubmitFilter = (filterParmas) => {
    console.log(filterParmas);
    this.getRecommends(filterParmas);
  }
  componentDidMount() {
    if (Platform.OS == 'ios') {
      const {StatusBarManager} = NativeModules;
      StatusBarManager.getHeight(statusBar =>
        this.setState({statusBarHeight: statusBar.height}),
      );
    }
    this.getRecommends();
  }
  render() {
    const {statusBarHeight, recommends} = this.state; 
    return (
      <ImageHeaderScrollView
        maxHeight={pxToDp(130) + statusBarHeight}
        minHeight={pxToDp(44) + statusBarHeight}
        headerImage={require('../../../res/images/headfriend.png')}
        renderForeground={() => (
          <View
            style={{
              height: pxToDp(130) + statusBarHeight,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <StatusBar
              backgroundColor={'transparent'}
              translucent={true}></StatusBar>
            <FriendHead></FriendHead>
          </View>
        )}>
        <View>
          <Vistiors></Vistiors>
          <PerfectGirl />
          <View>
            <View
              style={{
                height: pxToDp(40),
                backgroundColor: '#ccc',
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingLeft: pxToDp(10),
                paddingRight: pxToDp(10),
                alignItems: 'center',
              }}>
              <Text style={{color: '#666'}}>推荐</Text>
              <IconFont onPress={this.recommendFilterShow} name="iconshaixuan" style={{color: '#666'}} />
            </View>
            <View>
              {recommends.map((v, i) => (
                <TouchableOpacity
                  onPress={() => this.context.navigate("Detail",{id: v.id})}
                  key={i}
                  style={{
                    flexDirection: 'row',
                    padding: pxToDp(15), 
                    justifyContent: 'space-between',
                    alignItems: 'center', 
                    borderBottomWidth: 1,
                    borderColor: '#eee',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                    }}>
                    <Image
                      style={{
                        width: pxToDp(50),
                        height: pxToDp(50),
                        borderRadius: pxToDp(25),
                      }}
                      source={{uri: BASE_URI + v.header}}
                    />
                    <View
                      style={{
                        flexDirection: 'column',
                        justifyContent: 'space-around',
                        paddingLeft: pxToDp(5),
                      }}>
                      <View
                        style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={{color: '#555'}}>{v.nick_name}</Text>
                        <IconFont
                          name={
                            v.gender === '女' ? 'icontanhuanv' : 'icontanhuanan'
                          }
                          style={{
                            fontSize: pxToDp(18),
                            color: v.gender === '女' ? '#b564bf' : 'red',
                          }}
                        />
                        <Text style={{color: '#555'}}>{v.age}岁</Text>
                      </View>
                      <View style={{flexDirection: 'row'}}>
                        <Text style={{color: '#555'}}>
                          {v.marry}|{v.xueli}|
                          {v.agediff < 10 ? '年龄相仿' : '有点代沟'}
                        </Text>
                      </View>
                    </View>
                  </View>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <IconFont
                      name="iconxihuan"
                      style={{color: 'red', fontSize: pxToDp(22)}}></IconFont>
                    <Text>{v.fateValue}</Text>
                  </View>
                </TouchableOpacity>
              ))}
            </View>
          </View>
        </View>
      </ImageHeaderScrollView>
    );
  }
}
export default Index;
