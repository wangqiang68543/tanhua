import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {pxToDp} from '../../../utils/stylesKits';
import {SvgXml} from 'react-native-svg';
import {male, female} from '../../../res/fonts/iconSvg';
import {Input} from 'react-native-elements';
import DateTimePicker from '@react-native-community/datetimepicker';
import DatePicker from 'react-native-datepicker';
import Geo from '../../../utils/Geo';
import Picker from 'react-native-picker';
import CityJson from '../../../res/citys.json';

import THButton from '../../../components/THButton';
import Toast from '../../../utils/Toast';
import ImagePicker from 'react-native-image-crop-picker';
import {Image} from 'react-native';
import {Overlay} from 'teaset';
import {inject, observer} from 'mobx-react';
import request from '../../../utils/request';
import {ACCOUNT_CHECKHEADIMAGE, USER_REGINFO} from '../../../utils/pathMap';

import JMessage from '../../../utils/JMessage';
@inject('RootStore')
@observer
class Index extends Component {
  state = {
    nickname: '',
    gender: '男',
    birthday: new Date(),
    city: '',
    header: '',
    lng: '',
    lat: '',
    address: '',
    isShowDatePicker: false,
  };
  async componentDidMount() {
    console.log(this.props.RootStore);
    const res = await Geo.getCityByLocation();
    const address = res.regeocode.addressComponent.formatted_address;
    const city = res.regeocode.addressComponent.city.replace('市', '');
    const lng =
      res.regeocode.addressComponent.streetNumber.location.split(',')[0];
    const lat =
      res.regeocode.addressComponent.streetNumber.location.split(',')[0];
    this.setState({address, city, lng, lat});
    console.log(res);
  }
  chooseGender = gender => {
    this.setState({gender});
  };
  onDateChange = (event, selectedDate) => {
    console.log(selectedDate);
    this.setState({birthday: selectedDate});
  };
  // const onChange = (event, selectedDate) => {
  //   const currentDate = selectedDate || date;
  //   setShow(Platform.OS === 'ios');
  //   setDate(currentDate);
  // };

  showCityPicker = () => {
    console.log('============');
    Picker.init({
      pickerData: CityJson,
      selectedValue: ['北京', '北京'],
      wheelFlex: [1, 1, 0],
      pickerConfirmBtnText: '确定',
      pickerCancelBtnText: '取消',
      pickerTitleText: '选择城市',
      onPickerConfirm: data => {
        console.log(data);
        this.setState({city: data[1]});
      },
      onPickerCancel: data => {
        console.log(data);
      },
      onPickerSelect: data => {
        console.log(data);
      },
    });
    Picker.show();
  };
  jgBusiness = async (username, password) => {
    return JMessage.register(username, password);
  };
  chooseHeadImg = async () => {
    // const { nickname, birthday, city} = this.state;
    // if(!nickname || !birthday || !city){
    //   Toast.sad("昵称或者生日或者城市不合法", 2000, "center");
    //   return;
    // }
    const img = await ImagePicker.openPicker({
      width: pxToDp(300),
      height: pxToDp(400),
      cropping: true,
    });
    console.log(img);

    let overlayViewRef = null;
    let overlayView = (
      <Overlay.View
        style={{
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#000',
        }}
        modal={true}
        overlayOpacity={0}
        ref={v => (overlayViewRef = v)}>
        <View
          style={{
            marginTop: pxToDp(30),
            alignSelf: 'center',
            width: pxToDp(334),
            height: pxToDp(334),
            position: 'relative',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            style={{
              width: '100%',
              height: '100%',
              position: 'absolute',
              left: 0,
              top: 0,
              zIndex: 100,
            }}
            source={require('../../../res/images/scan.gif')}></Image>
          <Image
            style={{width: '60%', height: '60%', position: 'relative'}}
            source={{uri: img.path}}></Image>
        </View>
      </Overlay.View>
    );
    Overlay.show(overlayView);
    let formData = new FormData();
    formData.append('headPhoto', {
      uri: img.path,
      type: img.mime,
      name: img.path.split('/').pop(),
    });

    const res0 = await request.privatePost(ACCOUNT_CHECKHEADIMAGE, formData, {
      Headers: {
        'Content-Type': 'multipart-form-data',
      },
    });
    console.log(res0);
    if (res0 !== '10000') {
      return;
    }

    let parmas = this.state;
    parmas.header = res0.data.headImgPath;
    let birthday = this.state.birthday;
    var Y = birthday.getFullYear();
    var M = birthday.getMonth() + 1;
    var D = birthday.getDate();
    var times = Y + (M < 10 ? '-0' : '-') + M + (D < 10 ? '-0' : '-') + D;
    parmas.birthday = times;
    console.log(parmas);

    const res1 = await request.privatePost(USER_REGINFO, parmas);

    if (res1.code !== '10000') {
      console.log(res1);
      return;
    }
    const res2 = this.jgBusiness(
      this.props.RootStore.userId,
      this.props.RootStore.mobile,
    );

    overlayViewRef.close();

    Toast.smile('恭喜操作成功', 2000, 'center');
    setTimeout(() => {
      //跳转页面
    }, 2000);
  };
  render() {
    const {gender, nickname, birthday, isShowDatePicker, city} = this.state;
    // var selectDate = new Date()
    // if (birthday.length >= 0) {
    //   selectDate = new Date(birthday);
    // }
    // console.log(selectDate);
    // const dateNow = new Date();
    // const currentDate = `${dateNow.getFullYear()}-${dateNow.getMonth()+1}-${dateNow.getDate()}`;
    return (
      <View style={{backgroundColor: '#fff', flex: 1, padding: pxToDp(20)}}>
        <Text style={{fontSize: pxToDp(20), color: '#666', fontWeight: 'bold'}}>
          提升资料
        </Text>
        <Text style={{fontSize: pxToDp(20), color: '#666', fontWeight: 'bold'}}>
          提升魅力
        </Text>
        <View style={{marginTop: pxToDp(20)}}>
          <View
            style={{
              width: '60%',
              flexDirection: 'row',
              alignSelf: 'center',
              justifyContent: 'space-around',
            }}>
            <TouchableOpacity
              style={{
                height: pxToDp(60),
                width: pxToDp(60),
                borderRadius: pxToDp(30),
                backgroundColor: gender === '男' ? 'red' : '#eee',
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={this.chooseGender.bind(this, '男')}>
              <SvgXml xml={male} width="34" height="34"></SvgXml>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                height: pxToDp(60),
                width: pxToDp(60),
                borderRadius: pxToDp(30),
                backgroundColor: gender === '女' ? 'red' : '#eee',
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={this.chooseGender.bind(this, '女')}>
              <SvgXml xml={female} width="34" height="34"></SvgXml>
            </TouchableOpacity>
          </View>
          <View style={{marginTop: pxToDp(20)}}>
            <Input
              style={{height: pxToDp(44)}}
              value={nickname}
              placeholder="设置昵称"
              onChangeText={nickname => this.setState({nickname})}></Input>
          </View>

          {/* 日期 */}
          <View style={{paddingLeft: pxToDp(10), paddingRight: pxToDp(10)}}>
            <TouchableOpacity
              onPress={() => this.setState({isShowDatePicker: true})}
              style={{
                width: '100%',
                justifyContent: 'center',
                alignItems: 'flex-start',
                borderBottomWidth: pxToDp(1.1),
                borderBottomColor: '#999',
                height: pxToDp(44),
              }}>
              {/* {(isShowDatePicker ?  */}
              <DateTimePicker
                testID="dateTimePicker"
                value={birthday}
                mode="date"
                maximumDate={new Date()}
                minimumDate={new Date(1900, 0, 1)}
                display="compact"
                onChange={this.onDateChange}
                style={{width: '100%', flex: 1, color: '#000'}}
              />
              {/* : 
              <Text style={{fontSize: pxToDp(20), color:"#888"}}>设置生日</Text>)} */}
            </TouchableOpacity>
          </View>

          <View style={{marginTop: pxToDp(15)}}>
            <TouchableOpacity onPress={this.showCityPicker}>
              <Input
                style={{height: pxToDp(44)}}
                value={'当前定位:' + city}
                inputStyle={{color: '#666'}}
                disabled
              />
            </TouchableOpacity>
          </View>

          <View>
            <THButton
              style={{
                borderRadius: pxToDp(20),
                height: pxToDp(40),
                width: '85%',
                alignSelf: 'center',
              }}
              onPress={this.chooseHeadImg}>
              设置头像
            </THButton>
          </View>
        </View>
      </View>
    );
  }
}
export default Index;
