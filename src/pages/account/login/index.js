import React, {Component} from 'react';
import {View, Text, Image, StatusBar, StyleSheet, Ay} from 'react-native';
import {pxToDp} from '../../../utils/stylesKits';
import {Input} from 'react-native-elements';
import validator from '../../../utils/validator';
import request from '../../../utils/request';
import {ACCOUNT_LOGIN, ACCOUNT_VALIDATEVCODE } from '../../../utils/pathMap';
import THButton from '../../../components/THButton';
import Toast from '../../../utils/Toast';

import {CodeField, Cursor} from 'react-native-confirmation-code-field';

import {inject, observer} from 'mobx-react';
import AsyncStorage from '@react-native-async-storage/async-storage';


@inject("RootStore")
@observer
class Index extends Component {
  state = {
    // phoneNumber: '18665711978', //有推荐的账号
    phoneNumber: '13555346788',
    phoneValid: true,
    showLogin: true,
    vcodeTxt: '',
    buttonText: '重新获取', 
    isCountDowning: false,
  };
  phoneNumberChange = phoneNumber => {
    this.setState({phoneNumber});
  };
  phoneNumberSubmitEditing = async () => {
    const {phoneNumber} = this.state;
    const phoneValid = validator.validatePhone(phoneNumber);
    this.setState({phoneValid});
    if (!phoneValid) {
      return;
    }
    const res = await request.post(ACCOUNT_LOGIN, {phone: phoneNumber});
    if (res.code == '10000') {
      this.setState({showLogin: false});
      this.countDown();
      console.log(res)
    } else {
      Toast.message("获取验证码失败");
    }
    console.log(res);
  };
  handleGetVcode = () => {
    // if (this.state.vcodeTxt.length == 6) {
      if(!this.state.phoneValid) {
        this.phoneNumberSubmitEditing();
      } else {
        if(this.state.vcodeTxt.length == 6) {
          this.onVcodeSubmitEditing();
        } else {
          this.setState({showLogin: false})
          this.countDown();
        }
      }
      
    // } else {
    //   this.setState({showLogin: false})
    //   this.countDown();
    // } 
  };
  countDown=()=> {
    console.log("点击了按钮")
    let seconds=5;
    this.setState({buttonText:`重新获取(${seconds}s)`, isCountDowning: true});
    let timeId=setInterval(() => {
        seconds--;
        this.setState({buttonText:`重新获取(${seconds}s)`});
        if(seconds === 0) {
          clearInterval(timeId);
          this.setState({buttonText:`重新获取`, isCountDowning: false});
        }
    }, 1000);
  };
  renderLogin = () => {
    const {phoneNumber, phoneValid} = this.state;
    return (
      <View style={{padding: pxToDp(20)}}>
        <View>
          <View>
            <Text
              style={{
                fontWeight: 'bold',
                color: '#888',
                fontSize: pxToDp(25),
              }}>
              手机号码登录注册
            </Text>
          </View>
        </View>
        <View style={{marginTop: pxToDp(30)}}>
          <Input
            placeholder="请输入手机号"
            maxLength={11}
            keyboardType="phone-pad"
            value={phoneNumber}
            inputStyle={{color: '#333'}}
            onChangeText={this.phoneNumberChange}
            // onSubmitEditing={this.phoneNumberSubmitEditing}
            errorMessage={phoneValid ? '' : '手机号码不正确'}
            leftIcon={{
              type: 'font-awesome',
              name: 'phone',
              color: '#cccccc',
              size: pxToDp(20),
            }}
          />
        </View>
        <View>
          <THButton
            style={{
              borderRadius: pxToDp(20),
              height: pxToDp(40),
              width: '85%',
              alignSelf: 'center',
            }}
            onPress={this.handleGetVcode}>
            获取验证码
          </THButton>
        </View>
      </View>
    );
  };
  onVCodeChangeTxt = (vcodeTxt) => {
    console.log(vcodeTxt);
    this.setState({vcodeTxt});
  };
  onVcodeSubmitEditing = async () => {
      const {vcodeTxt, phoneNumber} = this.state;
      if (vcodeTxt.length!=6) {
        Toast.message("验证码不正确",2000,"center");
        return;
      }
      console.log(vcodeTxt);
      const res = await request.post(ACCOUNT_VALIDATEVCODE, {
        phone: '13555346788',//phoneNumber+""
        vcode: '888888', //vcodeTxt+""
      }, {headers: { 
        'Content-Type': ['application/json', 'text/plain']
      }});
      /**
       * , {headers: { 
        'Content-Type': 'application/x-www-form-urlencoded'
      }}
       */
      // 'Content-Type': ['application/json', 'charset=utf-8']
      //'Content-Type': ['application/json', 'text/plain'] 
      console.log(res);
      if (res.code != '10000') {
          return;
      } 
      this.props.RootStore.setUserInfo(phoneNumber,res.data.token,res.data.id);
      AsyncStorage.setItem("UserInfo", JSON.stringify({
        mobile: phoneNumber,
        token: res.data.token,
        userId: res.data.id,
      }));

      if (res.data.isNew) {
        //新用户
        this.props.navigation.navigate("UserInfo");
      } else {
        // 老用户 跳转到交友页面
        this.props.navigation.navigate("TabBar");
      }
  };

  renderCode = () => {
    const {phoneNumber, phoneValid, vcodeTxt, isCountDowning, buttonText} = this.state;
    return (
      <View style={{padding: pxToDp(20)}}>
        <View style={{marginTop: pxToDp(15)}}>
          <Text style={{color: '#888'}}>已发送到:+86 {phoneNumber}</Text>
        </View>
        <View style={{marginTop: pxToDp(15)}}>
          <View>
            {/* <Text style={{fontSize:pxToDp(25),color:"#888",fontWeight:"bold"}}>输入6位验证码</Text> */}
            <CodeField
              // Use `caretHidden={false}` when users can't paste a text value, because context menu doesn't appear
              value={vcodeTxt}
              onChangeText={this.onVCodeChangeTxt}
              onSubmitEditing={this.onVcodeSubmitEditing}
              cellCount={6}
              rootStyle={styles.codeFieldRoot}
              keyboardType="number-pad"
              renderCell={({index, symbol, isFocused}) => (
                <Text
                  key={index}
                  style={[styles.cell, isFocused && styles.focusCell]}>
                  {symbol || (isFocused ? <Cursor /> : null)}
                </Text>
              )}
            />
          </View>
          <View style={{marginTop: pxToDp(15)}}>
          <THButton 
            style={{
              borderRadius: pxToDp(20),
              height: pxToDp(40),
              width: '85%',
              alignSelf: 'center',
            }}
            disabled={isCountDowning} 
            onPress={this.handleGetVcode}>
            {this.state.vcodeTxt.length == 6 ? "登录" : buttonText}
          </THButton>
          </View>
        </View>
      </View>
    );
  };
  render() {
    const {showLogin} = this.state;
    return (
      <View>
        <StatusBar backgroundColor="transparent" translucent={true}></StatusBar>
        <Image
          style={{width: '100%', height: pxToDp(200)}}
          source={require('../../../res/images/profileBackground.jpg')}
        />

        {showLogin ? this.renderLogin() : this.renderCode()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {flex: 1, padding: 20},
  title: {textAlign: 'center', fontSize: 30},
  codeFieldRoot: {marginTop: 20},
  cell: {
    width: 40,
    height: 40,
    lineHeight: 38,
    fontSize: 24,
    borderColor: '#7d53ea',
    textAlign: 'center',   
    borderWidth: 2, 
    color: '#7d53ea',
  },
  focusCell: {
    borderColor: '#7d53ea',
    color: '#7d53ea',
  },
});

export default Index;
