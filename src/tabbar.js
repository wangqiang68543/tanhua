import React, {Component} from 'react';
import {View, Text} from 'react-native';
import TabNavigator from 'react-native-tab-navigator';
import {SvgXml} from 'react-native-svg';
import {
  friend,
  selectedFriend,
  group,
  selectedGroup,
  message,
  selectedMessage,
  my,
  selectedMy,
} from '../src/res/fonts/iconSvg';
import Friend from './pages/friend/home';
import Group from './pages/group/home';
import Message from './pages/message/home';
import My from './pages/my/home';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import request from './utils/request';
import {MY_USERINFO } from './utils/pathMap';
import JMessage from './utils/JMessage';
// import { inject, observer } from 'mobx-react';

const Tab = createBottomTabNavigator();

import {inject, observer} from 'mobx-react';

@inject('UserStore')
@observer
class Index extends Component {
  state = {
    selectedTab: 'friend',
    selectedIdx: 0,
    items: [
      {
        name: 'Friend',
        title: '交友',
        icon: friend,
        selectedIcon: selectedFriend,
        component: Friend,
      },
      {
        title: '圈子',
        name: 'Group',
        icon: group,
        selectedIcon: selectedGroup,
        component: Group,
      },
      {
        title: '消息',
        name: 'Message',
        icon: message,
        selectedIcon:selectedMessage,
        component: Message,
      },
      {
        title: '我的',
        name: 'My',
        icon: my ,
        selectedIcon: selectedMy,
        component: My,
      },
    ],
    // pages: [
    //   {
    //     selected: 'friend',
    //     title: '交友',
    //     renderIcon: () => <SvgXml xml={friend} height="20" width="20" />,
    //     renderSelectedIcon: () => (
    //       <SvgXml xml={selectedFriend} height="20" width="20" />
    //     ),
    //     onPress: () => this.setState({selectedTab: 'friend'}),
    //     component: <Friend />,
    //   },
    //   {
    //     selected: 'group',
    //     title: '圈子',
    //     renderIcon: () => <SvgXml xml={group} height="20" width="20" />,
    //     renderSelectedIcon: () => (
    //       <SvgXml xml={selectedGroup} height="20" width="20" />
    //     ),
    //     onPress: () => this.setState({selectedTab: 'group'}),
    //     component: <Group />,
    //   },
    //   {
    //     selected: 'message',
    //     title: '消息',
    //     renderIcon: () => <SvgXml xml={message} height="20" width="20" />,
    //     renderSelectedIcon: () => (
    //       <SvgXml xml={selectedMessage} height="20" width="20" />
    //     ),
    //     onPress: () => this.setState({selectedTab: 'message'}),
    //     component: <Message />,
    //   },
    //   {
    //     selected: 'my',
    //     title: '我的',
    //     renderIcon: () => <SvgXml xml={my} height="20" width="20" />,
    //     renderSelectedIcon: () => (
    //       <SvgXml xml={selectedMy} height="20" width="20" />
    //     ),
    //     onPress: () => this.setState({selectedTab: 'my'}),
    //     component: <My />,
    //   },
    // ],
  };
  // render() {
  //   const {selectedTab, selectedIdx, pages} = this.state;
  //   return (
  //     <View style={{flex: 1, backgroundColor:"#fff"}}>
  //       <TabNavigator>
  //         {pages.map((v, i) => (
  //           <TabNavigator.Item
  //             key={i}
  //             // selected={selectedTab === v.selected}
  //             selected={selectedIdx == i}
  //             title={v.title}
  //             renderIcon={v.renderIcon}
  //             renderSelectedIcon={v.renderSelectedIcon}
  //             // onPress={v.onPress}
  //             onPress={() => this.setState({selectedIdx: i})}
  //             selectedTitleStyle={{color:"#c863b5"}}>
  //             {v.component}
  //           </TabNavigator.Item>
  //         ))}
  //       </TabNavigator>
  //     </View>
  //   );
  // }
  async componentDidMount() {
    const res = await request.privateGet(MY_USERINFO);
    this.props.UserStore.setUser(res.data);//guid
    await JMessage.login(res.data.guid,res.data.mobile);
    console.log(res);
  }
  render() {
    const {items} = this.state; 
    return (
      <Tab.Navigator
        tabBarOptions={{
          activeTintColor: '#c863b5',
          inactiveTintColor: '#9E9E9E',
        }}>
        {items.map((v, i) => ( 
          <Tab.Screen
            key={i}
            name={v.name}
            title={v.title}
            component={ v.component }
            options={{
              tabBarLabel: v.title,
              tabBarIcon: ({focused, color, size}) => {
                return <SvgXml xml={focused ? v.selectedIcon : v.icon} height={size} width={size}></SvgXml>
              }
            }}/>
        ))}
      </Tab.Navigator>
    );
  }
}

export default Index;
