// In App.js in a new project

import React, { Component } from 'react'; 
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack'; 
import Login from "./pages/account/login";
import UserInfo from "./pages/account/userInfo";
import TabBar from './tabbar';
import TanHua from './pages/friend/tanhua';
import TestSoul from './pages/friend/testSoul';
import {inject, observer} from 'mobx-react';
import Detail from './pages/friend/detail';  
import Chat from './pages/message/chat';
const Stack = createStackNavigator(); 

@inject("RootStore")
@observer
class Nav extends Component {
  
  constructor(props) {
    super(props);
    this.state={
      initialRouteName: this.props.RootStore.token ? "TabBar" : "Login"
    }
    
  }
  render() {
    const {initialRouteName} = this.state;
    console.log(initialRouteName);
    return (
      <NavigationContainer>
        {/* {initialRouteName} */}
        {/* <Stack.Navigator headerMode="none" initialRouteName="TanHua"> */}
        <Stack.Navigator headerMode="none" initialRouteName={initialRouteName}> 
          <Stack.Screen name= "Chat" component={Chat}></Stack.Screen>
          <Stack.Screen name= "Detail" component={Detail}></Stack.Screen>
          <Stack.Screen name= "TestSoul" component={TestSoul}></Stack.Screen>
          <Stack.Screen name= "TanHua" component={TanHua}></Stack.Screen>
          <Stack.Screen name= "TabBar" component={TabBar}></Stack.Screen>
          <Stack.Screen name= "UserInfo" component={UserInfo}></Stack.Screen>
          <Stack.Screen name="Login" component={Login}></Stack.Screen> 
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
} 
export default Nav;